//
//  ViewController.h
//  LibPigtailSampleApp
//
//  Created by Nick Garner on 9/12/14.
//  Copyright (c) 2014 Pignology, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Pigtail.h"

@interface ViewController : UIViewController <PigtailDelegate>
{
    Pigtail *pt;
    
    IBOutlet UITextField *ptIPAddress;
    IBOutlet UILabel *freqLabel;
    IBOutlet UILabel *modeLabel;
    IBOutlet UIActivityIndicatorView *discovering;
    IBOutlet UILabel *stateLabel;
    
    CAGradientLayer *gradient;
}

@property (strong, nonatomic) IBOutlet UITextField *ptIPAddress;
@property (strong, nonatomic) IBOutlet UILabel *freqLabel;
@property (strong, nonatomic) IBOutlet UILabel *modeLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *discovering;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;

@end
