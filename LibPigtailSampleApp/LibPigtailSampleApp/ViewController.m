//
//  ViewController.m
//  LibPigtailSampleApp
//
//  Created by Nick Garner on 9/12/14.
//  Copyright (c) 2014 Pignology, LLC. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize freqLabel, modeLabel, ptIPAddress, discovering, stateLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    pt = [[Pigtail alloc] initWithDelegate:self];
    
    [discovering setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor lightGrayColor] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showAlertWithTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}

#pragma mark - UI
-(IBAction)discover:(id)sender
{
    [ptIPAddress resignFirstResponder];
    [discovering setHidden:NO];
    [discovering startAnimating];
    [pt discover];
}

-(IBAction)connect:(id)sender
{
    if ([ptIPAddress.text length] == 0)
    {
        [self showAlertWithTitle:@"ERROR" message:@"No PT IP Address"];
        return;
    }
    
    [ptIPAddress resignFirstResponder];
    
    [pt connectToPigtail:ptIPAddress.text radioType:ELECRAFT];
}

-(IBAction)disconnect:(id)sender
{
    [ptIPAddress resignFirstResponder];
    [pt disconnect];
    
    modeLabel.text = @"";
    freqLabel.text = @"";
}

#pragma mark - Pigtail Delegate Methods
- (void)onPigtail:(Pigtail *)pt didReceiveFreq:(NSString *)freq
{
    NSLog(@"received freq: %@", freq);
    freqLabel.text = freq;
}

- (void)onPigtail:(Pigtail *)pt didReceiveMode:(NSString *)mode
{
    NSLog(@"received mode: %@", mode);
    modeLabel.text = mode;
}

- (void)onPigtail:(Pigtail *)pt didFindPigtail:(NSString *)ipAddress
{
    NSLog(@"found pigtail: %@", ipAddress);
    ptIPAddress.text = ipAddress;
    [discovering stopAnimating];
    [discovering setHidden:YES];
}

- (void)onPigtail:(Pigtail *)pt stateChangedTo:(NSString *)state
{
    stateLabel.text = state;
}

@end
