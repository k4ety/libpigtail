//
//  main.m
//  LibPigtailSampleApp
//
//  Created by Nick Garner on 9/12/14.
//  Copyright (c) 2014 Pignology, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
