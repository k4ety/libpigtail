//
//  Pigtail.h
//
//  Created by Nicholas Garner on 09/12/2014.
//  Copyright (c) 2014 Pignology, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ELECRAFT 0
#define OLDYAESU 1 //FT-8X7
#define ICOM 2
#define KENWOOD 3
#define NEWYAESU 4 //FT-450, FT-3k, FT-5k, etc.

@class Pigtail;
@class AsyncSocket;
@class AsyncUdpSocket;

@protocol PigtailDelegate
@optional

/**
 * Called when freq received from radio.
 * This will be a string representation of the freq in MHz.
 **/
- (void)onPigtail:(Pigtail *)pt didReceiveFreq:(NSString *)freq;

/**
 * Called when mode received from radio.
 **/
- (void)onPigtail:(Pigtail *)pt didReceiveMode:(NSString *)mode;

/**
 * Called when a Pigtail/Piglet is found
 **/
- (void)onPigtail:(Pigtail *)pt didFindPigtail:(NSString *)ipAddress;

/**
 * Called when a Pigtail/Piglet connection state changes
 **/
- (void)onPigtail:(Pigtail *)pt stateChangedTo:(NSString *)state;

@end


@interface Pigtail : NSObject
{
    id theDelegate;
    
    BOOL debug;
    
    AsyncSocket *socket;
    AsyncUdpSocket *udpSocket;
    
    NSMutableArray *connectedSockets;
    BOOL CONNECTED;
    NSInteger radioType;

    NSString *currFreq;
    NSString *currMode;
    
    NSInteger currentIndexOfThingToCheck;

    NSTimer *timerGetRadioInfo;
    
    //icom
    u_char civAddress;
}
@property (nonatomic, retain) AsyncSocket *socket;
@property (readonly) BOOL CONNECTED;
@property (assign) NSInteger radioType;
@property (nonatomic, retain) NSString *currFreq;
@property (nonatomic, retain) NSString *currMode;
@property (assign) u_char civAddress;

- (id)initWithDelegate:(id)delegate;
- (void) discover;
- (void) logmessage:(NSString *)msg;
- (void) getRadioInfo;
- (void)enableTX;
- (void)enableRX;
- (void)connectToPigtail:(NSString*)pigtailAddress radioType:(int)r;
- (void)disconnect;
- (void)sendData:(NSData*)data;

@end
