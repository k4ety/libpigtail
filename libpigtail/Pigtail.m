//
//  Pigtail.m
//
//  Created by Nicholas Garner on 09/12/2014.
//  Copyright (c) 2014 Pignology, LLC. All rights reserved.
//

#import "Pigtail.h"
#import "AsyncSocket.h"
#import "AsyncUdpSocket.h"
#import "ComponentsSeparatedByByte.h"


@implementation Pigtail

@synthesize socket, CONNECTED;
@synthesize radioType;
@synthesize currFreq, currMode;
@synthesize civAddress;


- (id)init
{
	return [self initWithDelegate:nil];
}

- (id)initWithDelegate:(id)delegate
{
	if (self = [super init]) {
        theDelegate = delegate;
        
        debug = YES;
        
        connectedSockets = [[NSMutableArray alloc] initWithCapacity:1];
        CONNECTED = NO;
        socket = [[AsyncSocket alloc] initWithDelegate:self];
        
        currFreq = [[NSString alloc] init];
        currMode = [[NSString alloc] init];
        
        radioType = -1;
        
        civAddress = 0x00;
        
        [self logmessage:@"Pigtail init'd"];
    }
    return self;
}

#pragma mark - Start/Stop
-(void)connectToPigtail:(NSString*)pigtailAddress radioType:(int)type
{
    if (theDelegate == NULL)
	{
        [self showAlertWithTitle:@"ERROR" message:@"Delegate not set."];
        return;
	}
    
    if (type < 0)
    {
        [self showAlertWithTitle:@"Error" message:@"Radio type not defined."];
        return;
    }
    
    radioType = type;
    
    if (radioType == ICOM && civAddress == 0x00)
    {
        [self showAlertWithTitle:@"Icom Error" message:@"Ci-V Address not Set"];
        return;
    }
    
    [self logmessage:@"PT Start"];
    [self connectToServer:pigtailAddress];
    
    [self stateChangedTo:@"Trying..."];
}

-(void)disconnect
{
    [self logmessage:@"PT Stop"];
    if ([socket isConnected])
        [socket disconnect];
    
    [self stopTimers];
}

-(void)startTimers
{
    [self logmessage:@"Starting Timers"];
    currentIndexOfThingToCheck = 0;

    timerGetRadioInfo = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                         target:self
                                                       selector:@selector(getRadioInfo)
                                                       userInfo:nil
                                                        repeats:YES];
}

-(void)stopTimers
{
    if (timerGetRadioInfo != nil)
    {
        [timerGetRadioInfo invalidate];
        timerGetRadioInfo = nil;
    }
}

#pragma mark - Methods
-(void)stateChangedTo:(NSString*)state
{
    if ([theDelegate respondsToSelector:@selector(onPigtail:stateChangedTo:)])
    {
        [theDelegate onPigtail:self stateChangedTo:state];
    }

}

-(void)sendData:(NSData*)data
{
    [socket writeData:data withTimeout:5 tag:-1];
}

- (void) getRadioInfo
{
    if (radioType == ELECRAFT || radioType == KENWOOD || radioType == NEWYAESU)
    {
        NSMutableString *command = [[NSMutableString alloc] initWithString:@"FA;MD;"];
        [self sendData:[command dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if (radioType == OLDYAESU)
    {
        char freqcommand[5];
        freqcommand[0] = 0x00;
        freqcommand[1] = 0x00;
        freqcommand[2] = 0x00;
        freqcommand[3] = 0x00;
        freqcommand[4] = 0x03;
        
        NSData *freqdata = [[NSData alloc] initWithBytes:freqcommand length:5];
        [self sendData:freqdata];
    }
    else if (radioType == ICOM)
    {
        /*
         * For Icom, alternate what's being checked with each timer tick
         * this could be done more efficiently.
         */
        
        if (currentIndexOfThingToCheck == 2)
        {
            currentIndexOfThingToCheck = 0;
        }
        
        char command[6];
        switch (currentIndexOfThingToCheck) {
            case 0:
                //get freq = FEFE58E003FD
                command[0] = 0xFE;
                command[1] = 0xFE;
                command[2] = (char)civAddress;
                command[3] = 0xE0;
                command[4] = 0x03;
                command[5] = 0xFD;
                break;
                
            case 1:
                //get mode = FEFE58E004FD
                command[0] = 0xFE;
                command[1] = 0xFE;
                command[2] = (char)civAddress;
                command[3] = 0xE0;
                command[4] = 0x04;
                command[5] = 0xFD;
                break;
        }
        
        NSData *data = [[NSData alloc] initWithBytes:command length:6];
        [self sendData:data];
    }
    
    currentIndexOfThingToCheck++;
}

-(void)parseData:(NSData*)data
{
    BOOL recvdFreq = NO;
    BOOL recvdMode = NO;
    
#pragma mark Elecraft
    if (radioType == ELECRAFT)
    {
        //split by ;
        NSArray *messages = [data componentsSeparatedByByte:0x3B];
        
        for (int i = 0; i < [messages count]; i++)
        {
            NSString *msg = [[NSString alloc] initWithData:[messages objectAtIndex:i] encoding:NSASCIIStringEncoding];
            if ([msg hasPrefix:@"FA"])
            {
                NSString *fnum = [[msg stringByReplacingOccurrencesOfString:@"FA" withString:@""] stringByReplacingOccurrencesOfString:@";" withString:@""];
                float ffloat = [fnum floatValue];
                ffloat = ffloat / 1000000;
                currFreq = [[NSString alloc] initWithFormat:@"%f", ffloat];
                recvdFreq = YES;
            }
            else if ([msg hasPrefix:@"MD"])
            {
                if ([msg hasPrefix:@"MD1"]) currMode = @"LSB";
                else if ([msg hasPrefix:@"MD2"]) currMode = @"USB";
                else if ([msg hasPrefix:@"MD3"]) currMode = @"CW";
                else if ([msg hasPrefix:@"MD4"]) currMode = @"FM";
                else if ([msg hasPrefix:@"MD5"]) currMode = @"AM";
                else if ([msg hasPrefix:@"MD6"]) currMode = @"DATA";
                else if ([msg hasPrefix:@"MD7"]) currMode = @"CW-REV";
                else if ([msg hasPrefix:@"MD9"]) currMode = @"DATA-REV";
                recvdMode = YES;
            }
        }
    }
#pragma mark Yaesu
    else if (radioType == OLDYAESU)
    {

            //CAT will always send 5 bytes, return if it's not 5 bytes long
            if ([data length] != 5)
            {
                //NSLog(@"data length not 5: %d", [data length]);
            }
            else
            {
                //NSLog(@"radioType is Yaesu, received data length: %d", [data length]);
                NSUInteger len = [data length];
                Byte *byteData = (Byte*)malloc(len);
                memcpy(byteData, [data bytes], len);
                //get mode from byteData
                switch (byteData[4]) {
                    case 0x00:
                        currMode = @"LSB";
                        break;
                    case 0x01:
                        currMode = @"USB";
                        break;
                    case 0x02:
                        currMode = @"CW";
                        break;
                    case 0x03:
                        currMode = @"CWR";
                        break;
                    case 0x04:
                        currMode = @"AM";
                        break;
                    case 0x06:
                        currMode = @"WFM";
                        break;
                    case 0x08:
                        currMode = @"FM";
                        break;
                    case 0x0A:
                        currMode = @"DIG";
                        break;
                    case 0x0C:
                        currMode = @"PKT";
                        break;
                    default:
                        break;
                }
                
                //get freq
                NSMutableString *tf = [[NSMutableString alloc] initWithFormat:@"%x%0.2x%0.2x%0.2x", byteData[0], byteData[1], byteData[2], byteData[3]];
                [tf appendFormat:@"0"];
                //NSLog(@"tf: %@", tf);
                float mhz = [tf floatValue] / 1000000;
                currFreq = [[NSString alloc] initWithFormat:@"%0.5f", mhz];
                
                recvdFreq = YES;
                recvdMode = YES;
                free(byteData);
            }
        
    }
    else if (radioType == NEWYAESU)
    {
        NSArray *messages = [data componentsSeparatedByByte:0x3B];
        for (int i = 0; i < [messages count]; i++)
        {
            NSString *msg = [[NSString alloc] initWithData:[messages objectAtIndex:i] encoding:NSASCIIStringEncoding];
            
            if ([msg hasPrefix:@"FA"])
            {
                NSString *fnum = [[msg stringByReplacingOccurrencesOfString:@"FA" withString:@""] stringByReplacingOccurrencesOfString:@";" withString:@""];
                float ffloat = [fnum floatValue];
                ffloat = ffloat / 1000000;
                currFreq = [[NSString alloc] initWithFormat:@"%f", ffloat];
                recvdFreq = YES;
            }
            else if ([msg hasPrefix:@"MD"])
            {
                if ([msg hasPrefix:@"MD01"]) currMode = @"LSB";
                else if ([msg hasPrefix:@"MD02"]) currMode = @"USB";
                else if ([msg hasPrefix:@"MD03"]) currMode = @"CW";
                else if ([msg hasPrefix:@"MD04"]) currMode = @"FM";
                else if ([msg hasPrefix:@"MD05"]) currMode = @"AM";
                else if ([msg hasPrefix:@"MD06"]) currMode = @"DATA";
                else if ([msg hasPrefix:@"MD07"]) currMode = @"CW-REV";
                else if ([msg hasPrefix:@"MD09"]) currMode = @"DATA-REV";
                recvdMode = YES;
            }
        }
    }
#pragma mark Icom
    else if (radioType == ICOM)
    {
        //icom - sends data proactively
        NSArray *messages = [data componentsSeparatedByByte:0xFD];
        //NSLog(@"messages count: %d", [messages count]);
        for (int i = 0; i < [messages count]; i++)
        {
            NSData *message = [messages objectAtIndex:i];
            //NSLog(@"message length: %d", [message length]);
            
            if ([message length] <= 5)
            {
                // since we split on 0xFD, anything that is < 5 bytes long doesn't help us, it's the command we sent, or NG/OK
            }
            else
            {
                NSUInteger len = [message length];
                Byte *byteData = (Byte*)malloc(len);
                memcpy(byteData, [message bytes], len);
                
                NSMutableString *tf;
                float mhz;
                
                //see what the command is
                switch (byteData[4]) {
                    case 0x00:
                        //radio is proactively telling us its freq
                        //NSLog(@"received proactive freq");
                        recvdFreq = YES;
                        tf = [[NSMutableString alloc] initWithFormat:@"%0.2x%0.2x%0.2x%0.2x", byteData[9], byteData[8], byteData[7], byteData[6]];
                        [tf appendFormat:@"00"];
                        mhz = [tf floatValue] / 1000000;
                        currFreq = [[NSString alloc] initWithFormat:@"%0.5f", mhz];
                        break;
                    case 0x01:
                        //radio is proactively telling us its mode
                        //NSLog(@"received proactive mode");
                        recvdMode = YES;
                        switch (byteData[5]) {
                            case 0x00:
                                currMode = @"LSB";
                                break;
                            case 0x01:
                                currMode = @"USB";
                                break;
                            case 0x02:
                                currMode = @"AM";
                                break;
                            case 0x03:
                                currMode = @"CW";
                                break;
                            case 0x04:
                                currMode = @"RTTY";
                                break;
                            case 0x05:
                                currMode = @"FM";
                                break;
                            case 0x06:
                                currMode = @"WFM";
                                break;
                            default:
                                break;
                        }
                        break;
                    case 0x03:
                        //radio is responding to our query for the current freq
                        //NSLog(@"received freq");
                        recvdFreq = YES;
                        tf = [[NSMutableString alloc] initWithFormat:@"%0.2x%0.2x%0.2x%0.2x", byteData[9], byteData[8], byteData[7], byteData[6]];
                        [tf appendFormat:@"00"];
                        mhz = [tf floatValue] / 1000000;
                        currFreq = [[NSString alloc] initWithFormat:@"%0.5f", mhz];
                        break;
                    case 0x04:
                        //radio is responding to our query of the current mode
                        //NSLog(@"received mode");
                        recvdMode =YES;
                        switch (byteData[5]) {
                            case 0x00:
                                currMode = @"LSB";
                                break;
                            case 0x01:
                                currMode = @"USB";
                                break;
                            case 0x02:
                                currMode = @"AM";
                                break;
                            case 0x03:
                                currMode = @"CW";
                                break;
                            case 0x04:
                                currMode = @"RTTY";
                                break;
                            case 0x05:
                                currMode = @"FM";
                                break;
                            case 0x06:
                                currMode = @"WFM";
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                free(byteData);
            }
        }
    }
#pragma mark Kenwood
    else if (radioType == KENWOOD)
    {
        //kenwood
        //split by ;
        NSArray *messages = [data componentsSeparatedByByte:0x3B];
        
        for (int i = 0; i < [messages count]; i++)
        {
            NSString *msg = [[NSString alloc] initWithData:[messages objectAtIndex:i] encoding:NSASCIIStringEncoding];
            if ([msg hasPrefix:@"FA"])
            {
                NSString *fnum = [[msg stringByReplacingOccurrencesOfString:@"FA" withString:@""] stringByReplacingOccurrencesOfString:@";" withString:@""];
                float ffloat = [fnum floatValue];
                ffloat = ffloat / 1000000;
                //NSLog(@"ffloat is %f", ffloat);
                currFreq = [[NSString alloc] initWithFormat:@"%f", ffloat];
                recvdFreq = YES;
            }
            else if ([msg hasPrefix:@"MD"])
            {
                if ([msg hasPrefix:@"MD1"]) currMode = @"LSB";
                else if ([msg hasPrefix:@"MD2"]) currMode = @"USB";
                else if ([msg hasPrefix:@"MD3"]) currMode = @"CW";
                else if ([msg hasPrefix:@"MD4"]) currMode = @"FM";
                else if ([msg hasPrefix:@"MD5"]) currMode = @"AM";
                else if ([msg hasPrefix:@"MD6"]) currMode = @"DATA";
                else if ([msg hasPrefix:@"MD7"]) currMode = @"CW-REV";
                else if ([msg hasPrefix:@"MD9"]) currMode = @"DATA-REV";
                recvdMode = YES;
            }
        }
    }
    
    if (recvdFreq == YES)
    {
        if ([theDelegate respondsToSelector:@selector(onPigtail:didReceiveFreq:)])
        {
            [theDelegate onPigtail:self didReceiveFreq:currFreq];
        }
    }
    
    if (recvdMode == YES)
    {
        if ([theDelegate respondsToSelector:@selector(onPigtail:didReceiveMode:)])
        {
            [theDelegate onPigtail:self didReceiveMode:currMode];
        }
    }
}

-(void)enableTX
{
    [self logmessage:@"enableTX"];
    if (radioType == ELECRAFT || radioType == KENWOOD || radioType == NEWYAESU)
    {
        [self sendData:[@"TX;" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if (radioType == OLDYAESU)
    {
        
    }
    else if (radioType == ICOM)
    {
        
    }
}

-(void)enableRX
{
    [self logmessage:@"enableRX"];
    if (radioType == ELECRAFT || radioType == KENWOOD || radioType == NEWYAESU)
    {
        [self sendData:[@"RX;" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if (radioType == OLDYAESU)
    {
        
    }
    else if (radioType == ICOM)
    {
        
    }
}

- (void) logmessage:(NSString *)msg
{
    if (debug) NSLog(@"%@", msg);
}

-(void) showAlertWithTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Comms Client
- (void)connectToServer:(NSString *)pigtailAddress
{
    [self logmessage:[NSString stringWithFormat:@"connectToServer(): %@:7373 radioType: %ld", pigtailAddress, (long)radioType]];
    
    //get civAddress in case it's an Icom, defaults for 0x58
    [self logmessage:[NSString stringWithFormat:@"civAddress as int: 0x%x", civAddress]];
    
	NSError *err;
    if ([socket connectToHost:pigtailAddress onPort:7373 withTimeout:10.0 error:&err] == NO)
	{
        [self logmessage:[NSString stringWithFormat:@"Unable to connect to %@:7373\n", pigtailAddress]];
	}
}

- (void)onSocket:(AsyncSocket *)sock didAcceptNewSocket:(AsyncSocket *)newSocket
{
	[connectedSockets addObject:newSocket];
}

- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)theport
{
	[self logmessage:[NSString stringWithFormat:@"Connected: %@:%d", host, theport]];
	
	CONNECTED = YES;
    [self stateChangedTo:@"Connected"];
    
    [sock readDataWithTimeout:-1 tag:0];
    
    [self startTimers];
}


- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    NSString *rmsg = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    [self logmessage:[NSString stringWithFormat:@"received: %@", rmsg]];
    
    [self parseData:data];
    
    //read some more
    [sock readDataWithTimeout:-1 tag:0];
}

- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    [sock readDataWithTimeout:-1 tag:0];
}

- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err
{
    [self logmessage:[NSString stringWithFormat:@"Error: %@.\n", [err localizedDescription]]];
    [self showAlertWithTitle:@"Pigtail Error" message:[err localizedDescription]];
    CONNECTED = NO;
    [self stateChangedTo:@"Disconnected"];
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock
{
    [self stopTimers];
    
	[connectedSockets removeObject:sock];
	[self logmessage:[NSString stringWithFormat:@"Pigtail Disconnected."]];
	CONNECTED = false;
    [self stateChangedTo:@"Disconnected"];
}

#pragma mark - Discover
- (void)discover
{
    udpSocket = [[AsyncUdpSocket alloc] initIPv4];
    [udpSocket setDelegate:self];
    
    NSError *error = nil;
    if (![udpSocket bindToPort:7373 error:&error])
    {
        [self showAlertWithTitle:@"ERROR" message:[NSString stringWithFormat:@"Error starting UDP server (bind): %@", error]];
        return;
    }
    
    [self stateChangedTo:@"Listening..."];
    
    [udpSocket receiveWithTimeout:30 tag:0];
}


- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock didReceiveData:(NSData *)data withTag:(long)tag fromHost:(NSString *)host port:(UInt16)port
{
    [self logmessage:[NSString stringWithFormat:@"received a packet from: %@", host]];

    if ([theDelegate respondsToSelector:@selector(onPigtail:didFindPigtail:)])
    {
        [theDelegate onPigtail:self didFindPigtail:host];
    }

    [self stateChangedTo:@"Found a Pig"];
	return YES;
}


@end
